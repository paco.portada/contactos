package com.example.contact;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.contact.adapter.ContactsAdapter;
import com.example.contact.databinding.ActivityMainBinding;
import com.example.contact.model.Contact;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ArrayList<Contact> contacts;
    RecyclerView rvContacts;
    Button addMoreButton;
    ActivityMainBinding binding;
    ContactsAdapter adapter;
    ContactsAdapter.OnItemClickListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Initialize contacts
        contacts = Contact.createContactsList(20);
        // Create adapter passing in the sample user data
        listener = new ContactsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Contact contact) {
                Toast.makeText(MainActivity.this, "Contacto seleccionado: " + contact.getName(), Toast.LENGTH_LONG).show();
            }
        };
        adapter = new ContactsAdapter(contacts, listener);
        // Attach the adapter to the recyclerview to populate items
        binding.rvContacts.setAdapter(adapter);
        // Set layout manager to position the items
        binding.rvContacts.setLayoutManager(new LinearLayoutManager(this));

        binding.addContact.setOnClickListener(this);

        //adapter.setOnItemClickListener(this);

        // That's all!
    }

    @Override
    public void onClick(View view) {
        // record this value before making any changes to the existing list
        int curSize = adapter.getItemCount();
        // Add a new contact
        // contacts.add(0, new Contact("Barney", true));
        // Notify the adapter that an item was inserted at position 0
        // adapter.notifyItemInserted(0);
        contacts.add(curSize, new Contact("Barney", true));
        adapter.notifyItemInserted(curSize);
    }

}
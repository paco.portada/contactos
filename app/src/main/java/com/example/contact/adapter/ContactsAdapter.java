package com.example.contact.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.example.contact.databinding.ItemContactBinding;
import com.example.contact.model.Contact;

import java.util.List;

/**
 * Adaptador que gestiona una lista de Contactos
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactViewHolder> {

    private List<Contact> list;
    private OnItemClickListener listener;

    //Interfaz para el evento onClick de un elemento del RecyclerView
    public interface OnItemClickListener {
        void onItemClick(Contact Contact);
    }

    //Constructor que se le pasa la lista de notas y el listener
    public ContactsAdapter(List<Contact> list, OnItemClickListener listener) {
        this.list = list;
        this.listener=listener;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // return new ContactViewHolder(ItemContactBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        ItemContactBinding binding = ItemContactBinding.inflate(inflater, parent, false);

        return new ContactViewHolder(binding);
    }

    //Método que vincula cada dato con la vista
    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        Contact Contact = list.get(position);
        holder.binding.contactName.setText(Contact.getName());
        holder.binding.messageButton.setText(Contact.isOnline() ? "Message" : "Offline");
        holder.binding.messageButton.setEnabled(Contact.isOnline());
        holder.bind(list.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * Clase ViewHolder
     */

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        ItemContactBinding binding;
        //public TextView title, content;

        public ContactViewHolder(ItemContactBinding b) {
            //super(view);
            //title = view.findViewById(R.id.title);
            //content = view.findViewById(R.id.content);
            super(b.getRoot());
            binding = b;
        }

        public void bind(final Contact Contact, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(Contact);
                }
            });
        }
    }
}

